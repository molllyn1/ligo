(library
  (name cli)
  (libraries
    simple-utils
    cmdliner
    ligo
    build
  )
  (modules cli cli_helpers formatter version changelog)
  (preprocess
    (pps ppx_let bisect_ppx --conditional)
  )
  (flags (:standard -open Simple_utils))
)

(rule
 (targets version.ml)
 (action (with-stdout-to
          version.ml
          (run "sh" "-c"
               "printf 'let version = \"%s\"' \"${LIGO_VERSION}\""))))

(rule
 (targets changelog.ml)
 (deps (env_var CHANGELOG_PATH))
 (action (with-stdout-to
          changelog.ml
          (run "sh" "-c" "printf 'let changelog = {changelog|%s|changelog}' \"$(cat ${CHANGELOG_PATH:-../../gitlab-pages/docs/intro/changelog.md})\""))))

; build static executable with --profile static
(env
 (static (flags (:standard -ccopt -static -cclib "-lgmp"))))

(executable
  (name runligo)
  (public_name ligo)
  (libraries
    simple-utils
    cmdliner
    ligo
    cli
  )
  (modules runligo)
  (package ligo)
  (preprocess
    (pps ppx_let bisect_ppx --conditional)
  )
  (flags (:standard -open Simple_utils))
)
